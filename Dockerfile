FROM node:alpine

RUN mkdir -p /usr/src/node-base-ms && chown -R node:node /usr/src/node-base-ms

WORKDIR /usr/src/node-base-ms

COPY package*.json ./

USER node

RUN npm install --pure-lockfile

COPY --chown=node:node . .

EXPOSE 3000
